CC := gcc
SRCS := $(wildcard *.c)

# Determine platform
UNAME := $(shell uname -s)
ifeq ($(UNAME), Linux)
	LINUX := 1
else ifeq ($(UNAME), Darwin)
	OSX := 1
endif

# Debug?
# DEBUG := 1

INCLUDE_DIRS := ./include /usr/local/include
LDFLAGS := -L/usr/local/lib
ifeq ($(LINUX), 1)
	COMMON_FLAGS := -std=gnu99
endif
ifeq ($(DEBUG), 1)
	COMMON_FLAGS += -g
endif
LIBRARIES := json-c pthread
DEPS := src/json-c/parse_json.d

.PHONY: clean

COMMON_FLAGS += $(foreach includedir, $(INCLUDE_DIRS), -I$(includedir))
CFLAGS += $(COMMON_FLAGS)
LDFLAGS += $(foreach library, $(LIBRARIES), -l$(library))
LDFLAGS += $(COMMON_FLAGS)

all: client server test # nbserver

%.o: %.c
	$(Q)$(CC) $(CFLAGS) -c $*.c -o $*.o
	$(Q)$(CC) $(CFLAGS) -MM $*.c > $*.d

%: %.o
	$(Q)$(CC) $< -o $@ $(CFLAGS)

server: server.o src/json-c/parse_json.o src/kv511/kv511.o src/kv511/kvlock.o
	$(Q)$(CC) $^ -o $@ $(CFLAGS) $(LDFLAGS)

client: client.o src/json-c/parse_json.o src/kv511/kv511.o src/kv511/kvlock.o
	$(Q)$(CC) $^ -o $@ $(CFLAGS) $(LDFLAGS)

client_multi_t_json: client_multi_t_json.o src/json-c/parse_json.o src/kv511/kv511.o
	$(Q)$(CC) $^ -o $@ $(CFLAGS) $(LDFLAGS)

test: test_cli.c
	$(Q)$(CC) $^ -o $@

clean:
	@rm -f client server nbserver test client_multi_t_json
	@rm -rf *.o *.d

-include $(DEPS)
