1. If your OS does not support json-c-0.12, please compile 3rdParty/json-c-0.12 & install it.
   The source codes use C99 features. You should use ./configure CFLAGS="-std=gnu99" for compilation.
   Prerequisite: autoconf, automake, gcc

2. enter “make” to compile.
   client_multi_t_json.c is a source code for multithreaded client.
   server.c is a source code for server. The type of server(single-threaded/multi-thread) can be set by server_env.json. They are all in written in library.

3. For verifying functional correctness in client-server model, you can try following steps:
   3-A. Execute ./server
        server configuration is located at json/server_env.json
   3-B. Execute ./test 127.0.0.1
        Test client just tries to send JSON Put/Get requests presented by json/testRequests.json
        *Because client is not ready to use at first, multi-thread server is tested in the following script
            Terminal-I  : for i in {1..10000}; do ./test 127.0.0.1; done
            Terminal-II : for i in {1..4000}; do ./test 127.0.0.1; done
            Termianl-III: for i in {1..4000}; do ./test 127.0.0.1; done
   3-C. Compare Results corresponding to request

We were run out of time and we could not finish the project.
