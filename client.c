#include <stdio.h>
#include "kv511.h"

int main(int argc, char *argv[]) {
    char *pConfigJSONFile = NULL;
    json_object *jobj = NULL;
    if (argc > 2)
    {
        printf("Usage: client [ConfigFilePath]\n");
        exit(EXIT_FAILURE);
    }
    else if(argc == 2)
    {
        pConfigJSONFile = argv[1];
    }
    else
    {
        pConfigJSONFile = "json/client_env.json";
    }

    jobj = set_env(pConfigJSONFile);
    if (jobj != NULL) {
        printf("%s\n", json_object_to_json_string(jobj));

        // get & put API example
        get(10, "test");
        put(10, "yrdy", jobj);

        // Release JSON object
        json_object_put(jobj);
    }
    // Following procedure
    // 1. open socket
    // 2. set session id
    // 3. send requests
    // 4. recv response
    // 5. estimate time
    // 6. close session
    // 7. etc

    return 0;
}