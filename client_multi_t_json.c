#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h> 
#include <arpa/inet.h> 
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <pthread.h>
#include "kv511.h"
#define MAXBUF    500 
#define PORT 9000
#define MAX_TH 100



int main(int argc, char **argv) 
{
    char *pConfigJSONFile =NULL;
    json_object *jobj =NULL;
    session_t servfd= -1;

    if (argc > 2)
    {
        printf("Usage: client [ConfigFilePath]\n");
        exit(EXIT_FAILURE);
    }
    else if(argc == 2)
    {
        pConfigJSONFile = argv[1];
    }
    else
    {
        pConfigJSONFile = "json/client_env.json";
    }    

    jobj = set_env(pConfigJSONFile);
    if (jobj != NULL) {
        json_object_put(jobj);
    }    

    int i;
    int t_num = get_numThreads();
    //t_num=init_client();

    for(i=1;i<=t_num;i++)
	{
        
        start_client(i);
     
        
	}	
   
	
  




        return 0;
} 
