#ifndef KV511_H__
#define KV511_H__

#include "parse_json.h"
#include "kvlock.h"

#define BUFLEN 256

typedef int session_t;
typedef char _key_t;
typedef json_object val_t;
typedef enum _enumkv511 {KV511CLI, KV511SVR, KV511SVR_STORE} enumkv511;

// Set environments from configuration and retrieve a list of JSON requests
json_object* set_env(const char *pJSONEnvFileName);

// Get information from library
int get_numThreads(void);

// Procedure for runnning server and client
session_t init_server(void);
void start_server(session_t servfd);
void close_server(session_t servfd);

// communication interface APIs
val_t* get(session_t sid, _key_t *k);
int put(session_t sid, _key_t *k, val_t *v);

#endif // KV511_H__