#ifndef KVLOCK_H__
#define KVLOCK_H__

#include <pthread.h>
#include <sys/queue.h>

/* delay handling */
#include <unistd.h>

#ifndef INLINE
# if __llvm__
#  define INLINE static inline
# elif __GNUC__ && !__GNUC_STDC_INLINE__
#  define INLINE extern inline
# else
#  define INLINE inline
# endif
#endif

STAILQ_HEAD(stailhead_t, queue_t);
struct queue_t {
    pthread_t tid;
    STAILQ_ENTRY(queue_t) entries;
};

typedef struct __lock_t {
    int guard;              // Due to limitation of spin on test_and_set, it is not a good idea to use pthread_mutex_t.
    pthread_t tParkId;
    pthread_mutex_t mutex;
    size_t szLastQuant;
    size_t szMaxQuant;
    useconds_t unitDelay;
    struct stailhead_t q;
} lock_t;

void lock_init(lock_t *m, unsigned long ulDelayUnit, size_t szMaxQuant);
void lock_close(lock_t *m);
void lock(lock_t *m);
void unlock(lock_t *m);

#endif // KVLOCK_H__