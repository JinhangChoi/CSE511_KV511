#ifndef PARSE_JSON_H__
#define PARSE_JSON_H__

#include <json-c/json.h>

void print_json_value(json_object *jobj);
void json_parse_array(json_object *jobj, char *key);
void json_parse(json_object *jobj);
json_object* json_getJobj(json_tokener *restrict tok, char *restrict pStr, size_t szStrLen, int *restrict nOffset, int bVerbose);

#endif // PARSE_JSON_H__