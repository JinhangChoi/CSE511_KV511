#!/usr/local/bin/python
import json
import csv
import random

fnameCSV = 'cse_programs.csv'
fnameJSON = 'sampleRequest'
cases = 1
reqsPerSession = 10
sessions = 10

# Print samples for A: requests / B: sessions 
justListOfReqs = True

key = []
dict = {}
with open(fnameCSV, 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        key.append(row[0])
        dict[row[0]] = row[1]

numOfCourses = len(key)
for i in range(0, cases):
    with open(fnameJSON + str(i) + '.json', 'w') as jsonfile:
        print fnameJSON + str(i) + '.json'
        for session in range(0, sessions):
            reqs = {}
            for req in range(0, reqsPerSession):
                tmpReq = {}
                idx = random.randint(0, 1)
                id = key[random.randint(0, numOfCourses-1)]
                if idx == 0:
                    tmpPut = {}
                    tmpPut[id]=dict[id]
                    tmpReq["put"] = tmpPut
                else:
                    tmpReq["get"] = id
                reqs[req] = tmpReq

                if justListOfReqs == True:
                    print tmpReq
                    jsonfile.write(json.dumps(tmpReq))
                    jsonfile.write('\n')

            if justListOfReqs == False:
                print reqs
                jsonfile.write(json.dumps(reqs))
                jsonfile.write('\n')