#!/usr/local/bin/python
import json
import csv

fname = 'cse_programs'
fnameCSV = fname+'.csv'
fnameJSON = fname+'.json'

dict = {}
with open(fnameCSV, 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        dict[row[0]] = row[1]

with open(fnameJSON, 'w') as jsonfile:
    jsonfile.write(json.dumps(dict))
