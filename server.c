#include <stdio.h>
#include "kv511.h"

int main(int argc, char *argv[]) {
    char *pConfigJSONFile = NULL;
    json_object *jobj = NULL;
    session_t servfd = -1;

    if (argc > 2)
    {
        printf("Usage: server [ConfigFilePath]\n");
        exit(EXIT_FAILURE);
    }
    else if(argc == 2)
    {
        pConfigJSONFile = argv[1];
    }
    else
    {
        pConfigJSONFile = "json/server_env.json";
    }

    jobj = set_env(pConfigJSONFile);
    if (jobj != NULL) {
        // Ignore requests
        json_object_put(jobj);
    }

    // Following procedure
    // 1. open socket (set session id)
    // 2. waiting for requests
    // 3. process requests
    // 4. close session
    // 5. etc
    servfd = init_server();

    if (servfd != -1) {
        start_server(servfd);
        close_server(servfd);
    }

    return 0;
}