#include <stdio.h>
#include "parse_json.h"

void print_json_value(json_object *jobj) {
    enum json_type type;
    type = json_object_get_type(jobj);
    printf("[%s] ", json_type_to_name(type));    
    switch(type) {
        case json_type_null:
            printf("%s\n", json_object_get_string(jobj));
            break;
        case json_type_boolean:
            printf("%s\n", json_object_get_boolean(jobj) ? "true" : "false");
            break;
        case json_type_int:
            printf("%d\n", json_object_get_int(jobj));
            break;
        case json_type_double:
            printf("%lf\n", json_object_get_double(jobj));
            break;
        case json_type_string:
            printf("%s\n", json_object_get_string(jobj));
            break;
        default:
            /* These are not value type */
            printf("\n");
            break;
    }
}

void json_parse_array(json_object *jobj, char *key) {
    void json_parse(json_object *jobj);
    enum json_type type;

    json_object *jarray = jobj;
    if (key) {
        json_object_object_get_ex(jobj, key, &jarray);
    }
    printf("test: %x", (unsigned int)key);
    int arraylen = json_object_array_length(jarray);
    printf("Array length: %d\n", arraylen);

    json_object *jvalue;
    for (int i=0; i < arraylen; i++) {
        jvalue = json_object_array_get_idx(jarray, i);
        type = json_object_get_type(jvalue);
        if (type == json_type_array) {
            json_parse_array(jvalue, NULL);
        }
        else if(type != json_type_object) {
            printf("value[%d]: ", i);
            print_json_value(jvalue);
        }
        else {
            json_parse(jvalue);
        }
    }
}

void json_parse(json_object *jobj) {
    enum json_type type;
    json_object_object_foreach(jobj, key, val) {
        type = json_object_get_type(val);
        printf("type: %s ", key);
        switch(type) {
            case json_type_null:
            case json_type_boolean:
            case json_type_int:
            case json_type_double:
            case json_type_string:
                print_json_value(val);
                break;
            case json_type_object:
                json_object_object_get_ex(jobj, key, &jobj);
                json_parse(jobj);
                break;
            case json_type_array:
                json_parse_array(jobj, key);
                break;
        }
    }
}

json_object* json_getJobj(json_tokener *restrict tok, char *restrict pStr, size_t szStrLen, int *restrict nOffset, int bVerbose) {

    json_object *jobj = NULL;
    enum json_tokener_error jerr;
    
    if (tok == NULL || pStr == NULL || nOffset == NULL) {
        return NULL;
    }
    jobj = json_tokener_parse_ex(tok, pStr, szStrLen);
    jerr = json_tokener_get_error(tok);
    if (bVerbose) {
        printf("[%s:%d] %s\n", __func__, __LINE__, json_tokener_error_desc(jerr));
    }
    if (jerr != json_tokener_success)
    {
        if (jobj != NULL) {
            printf("[%s:%d] Should not return partial object: %s\n", __func__, __LINE__, json_tokener_error_desc(jerr));
        }
        *nOffset = -1;
    }
    else if (jobj != NULL)
    {
        /* Success in parsing JSON */
        *nOffset = tok->char_offset;
    }
    else
    {
        printf("[%s:%d] Unexpected JSON Parsing Error\n", __func__, __LINE__);
        *nOffset = 0;
    }

    return jobj;
}