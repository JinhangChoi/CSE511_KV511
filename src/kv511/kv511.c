/* Headers for Memory & String */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* Headers for networking */
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>

/* Headers for multi-thread */
#include <pthread.h>
#include <sys/time.h>

#include "kv511.h"

/* buffer size and the max number of threads in client*/
#define MAXBUF 500
#define MAX_TH 100

/* Key-Value Store Cache structure */
#define MAX_KEYLEN 10
typedef struct {
    char key[MAX_KEYLEN];
    char *pVal;
    size_t szStoreLen;
    long long nTimeStamp;
} stkv511Cache, *pstkv511Cache;
static pstkv511Cache kv511 = NULL;

lock_t kvCacheLock;

/* default configurations */
static int bDebug = 0;
static enumkv511 kv511Type = KV511CLI;
static int nThreads = 1;
static size_t szMaxDelayQuant = 1;
static unsigned long ulLockDelayUnit = 0;   /* in microseconds */
static size_t szKV511Cache = 10;
static long long llLogicalTimeStamp = 0;

/* In server, the number of threads depends on nMaxSessions */
static int nMaxSessions = 10;

/* First quorum assumption : N < W+R */
static int nStoreReplica = 5;
static int nWriteQuorum = 4;
static int nReadQuorum = 2;

/* Server Address Information */
static int nServResource = 0;

/* IPv4-mapped IPv6 has 45 characters */
#define IPPORT_MAXLEN 6
typedef struct {
    char ipAddr[INET6_ADDRSTRLEN];
    char ipPort[IPPORT_MAXLEN];
} stAddrInfo, *pstAddrInfo;
static pstAddrInfo pstServIp = NULL;

struct request{
    int type;
    char key[8];
    char *value;
//    int time;   //closed-loop version
};

int get_numThreads(void) {
    return nThreads;
}

size_t trim_trailingWhiteSpace(char *pStr, size_t szStrLen) {
    int nPos = 0;
    if (pStr == NULL || szStrLen == 0){
        return 0;
    }
    nPos = szStrLen - 1;
    while(nPos > 0 && isspace(pStr[nPos])) nPos--;

    if (nPos < 0) {
        nPos = -1;
    }
    return nPos + 1;
}

char* get_strBuf(char *restrict pBuf, size_t *restrict pszBuf, int *restrict pnLastPos, char *restrict pNewline, size_t szNewline) {
    const size_t szIncBuf = BUFLEN;
    size_t szBufRemain = 0;
    char *pNew = NULL;
    int nLastPos = 0;

    if (pszBuf == NULL || pnLastPos == NULL || pNewline == NULL || szNewline == 0) {
        printf("[%s:%d] param error (pszBuf: %x, pnLastPos: %x, pNewline: %x, szNewline: %x)", \
            __func__, __LINE__, (unsigned int)pszBuf, (unsigned int)pnLastPos, (unsigned int)pNewline, (unsigned int)szNewline);
        return NULL;
    }

    if (pBuf == NULL) {
        szBufRemain = 0;
        nLastPos = 0;
    } else {
        nLastPos = *pnLastPos;
        szBufRemain = *pszBuf - nLastPos;
    }
    
    if (szBufRemain < szNewline) {
        size_t szNewBuf = nLastPos + szIncBuf;

        if (szNewline > szNewBuf) {
            szNewBuf = szNewline;
        }
        pNew = (char *)realloc(pBuf, sizeof(char) * szNewBuf);
        if (pNew != NULL) {
            *pszBuf = szNewBuf;
        } else {
            printf("[%s:%d] pNew realloc error", __func__, __LINE__);
        }
    } else {
        pNew = pBuf;
    }

    memcpy(pNew + nLastPos, pNewline, sizeof(char) * szNewline);
    *pnLastPos = nLastPos + szNewline;

    return pNew;
}

json_object* set_env(const char *pJSONEnvFileName) {
    FILE *fpConfig = NULL;
    json_tokener *tok = NULL;
    json_object *jobj = NULL;

    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    size_t szBuf = 0;
    char *pStrBuf = NULL;

    int bPosRst = 0;
    int nPrevPos = 0;
    int nLastPos = 0;
    int nOffset = -1;

    /* Open JSON configuration file */
    if ((fpConfig = fopen(pJSONEnvFileName, "r")) == NULL) {
        printf("[%s:%d] Error in opening %s\n", __func__, __LINE__, pJSONEnvFileName);
        return NULL;
    }
    
    /* Create JSON tokener : parsing rule is based on strict mode */
    if ((tok = json_tokener_new()) == NULL) {
        printf("[%s:%d] Error in create json tokener\n", __func__, __LINE__);
        return NULL;
    }
    json_tokener_set_flags(tok, JSON_TOKENER_STRICT);

    /* read each line from JSON configuration file */
    bPosRst = 1;
    while((read = getline(&line, &len, fpConfig)) != -1) {
        json_object *jobjTmp = NULL;
        /* 
         * At first, trim trailing whitespaces from string 
         * If there is no line, skip to the next line
         */
        size_t szStrTrim = trim_trailingWhiteSpace(line, strlen(line));
        if (szStrTrim == 0) continue;

        /*
         * JSON tokener does not take care of string buffer.
         * Therefore, we need to keep the entire string until JSON tokener returns JSON object.
         * CAVEAT: For each parsing step, we are going to feed each line instead of the entire string.
         */
        if (bPosRst) {
            nPrevPos = 0;
            bPosRst = 0;
        } else {
            nPrevPos = nLastPos;
        }
        pStrBuf = get_strBuf(pStrBuf, &szBuf, &nLastPos, line, szStrTrim);
        jobjTmp = json_getJobj(tok, pStrBuf + nPrevPos, nLastPos - nPrevPos, &nOffset, bDebug);
        if (jobjTmp != NULL) {
            /*
             * If JSON object is created by JSON tokener,
             * we do not need to keep the original string.
             * Restore buffer space for the following strings.
             */
            enum json_type type;
            int nRemain = nLastPos - nPrevPos - nOffset;
            memmove(pStrBuf, pStrBuf + nPrevPos + nOffset, nRemain);
            nLastPos = nRemain;

            json_object_object_foreach(jobjTmp, key, value) {
                type = json_object_get_type(value);
                if (strcmp(key, "type") == 0) {
                    if (type == json_type_string) {
                        if(strcmp(json_object_get_string(value), "server-store") == 0) {
                            kv511Type = KV511SVR_STORE;
                        } else if (strcmp(json_object_get_string(value), "server") == 0) {
                            kv511Type = KV511SVR;
                        } else {
                            if (strcmp(json_object_get_string(value), "client") != 0) {
                                printf("[%s:%d] Force to use it as client (UnknownType: %s)\n", __func__, __LINE__, json_object_get_string(value));                                
                            }
                            kv511Type = KV511CLI;
                        }
                    }
                } else if (strcmp(key, "threads") == 0) {
                    if (type == json_type_int) {
                        nThreads = json_object_get_int(value);
                    }
                } else if (strcmp(key, "serv_info") == 0) {
                    // If this is client/store, ipaddr[0] indicates server's front-end ip address
                    // & port indicates server's listening port
                    // Otherwise, ignore.
                    if (type == json_type_object) {
                        json_object *jServInfo = json_object_get(value);
                        json_object_object_foreach(jServInfo, servKey, servValue) {
                            type = json_object_get_type(servValue);
                            if (strcmp(servKey, "ipaddr") == 0) {
                                if (type == json_type_string && json_object_get_string_len(servValue) <= INET6_ADDRSTRLEN) {
                                    if (pstServIp == NULL) {
                                        pstServIp = (pstAddrInfo)malloc(sizeof(stAddrInfo));
                                        memset(pstServIp, 0, sizeof(stAddrInfo));
                                    }
                                    if (pstServIp != NULL) {
                                        sprintf(pstServIp[0].ipAddr, "%s", json_object_get_string(servValue));
                                    }
                                    if (nServResource < 1) nServResource = 1;
                                } else if(type == json_type_array) {
                                    int arraylen = json_object_array_length(servValue);
                                    if (pstServIp == NULL) {
                                        pstServIp = (pstAddrInfo)malloc(sizeof(stAddrInfo) * arraylen);
                                        memset(pstServIp, 0, sizeof(stAddrInfo) * arraylen);
                                    }
                                    for(int i=0; pstServIp != NULL && i < arraylen; i++) {
                                        json_object *jvalue = json_object_array_get_idx(servValue, i);
                                        type = json_object_get_type(jvalue);
                                        if (type == json_type_string) {
                                            sprintf(pstServIp[i].ipAddr, "%s", json_object_get_string(jvalue));
                                        }
                                    }
                                    if (nServResource < arraylen) nServResource = arraylen;                                    
                                }
                            } else if(strcmp(servKey, "port") == 0) {
                                type = json_object_get_type(servValue);
                                if (type == json_type_int) {
                                    if (pstServIp == NULL) {
                                        pstServIp = (pstAddrInfo)malloc(sizeof(stAddrInfo));
                                        memset(pstServIp, 0, sizeof(stAddrInfo));
                                    }
                                    if (pstServIp != NULL) {
                                        sprintf(pstServIp[0].ipPort, "%s", json_object_get_string(servValue));
                                    }
                                    if (nServResource < 1) nServResource = 1;
                                } else if(type == json_type_array) {
                                    int arraylen = json_object_array_length(servValue);
                                    if (pstServIp == NULL) {
                                        pstServIp = (pstAddrInfo)malloc(sizeof(stAddrInfo) * arraylen);
                                        memset(pstServIp, 0, sizeof(stAddrInfo) * arraylen);
                                    }
                                    for(int i=0; pstServIp != NULL && i < arraylen; i++) {
                                        json_object *jvalue = json_object_array_get_idx(servValue, i);
                                        type = json_object_get_type(jvalue);
                                        if (type == json_type_int) {
                                            sprintf(pstServIp[i].ipPort, "%s", json_object_get_string(jvalue));
                                        }
                                    }
                                    if (nServResource < arraylen) nServResource = arraylen;
                                }
                            }

                            if (pstServIp == NULL) {
                                perror("set_env: malloc(pstServIp)");
                                exit(EXIT_FAILURE); 
                            }
                        }
                    }
                } else if (strcmp(key, "max_session") == 0) {
                    // How many session(socket) can we use at the same time?
                    if (type == json_type_int) {
                        nMaxSessions = json_object_get_int(value);
                    }
                } else if (strcmp(key, "cache_size") == 0) {
                    // How many session(socket) can we use at the same time?
                    if (type == json_type_int) {
                        szKV511Cache = json_object_get_int(value);
                    }
                } else if (strcmp(key, "request") == 0) {
                    // Extract request JSON value and deliver it to callee
                    jobj = json_object_get(value);
                } else if (strcmp(key, "debug") == 0) {
                    // Set debug mode for kv511 library
                    if (type == json_type_boolean) {
                        bDebug = json_object_get_boolean(value);
                    }
                } else if (strcmp(key, "lock_delay") == 0) {
                    // Set delay information for kv511 locks
                    if (type == json_type_array) {
                        int arraylen = json_object_array_length(value);
                        if (arraylen == 2) {
                            json_object *jvalue = NULL;
                            jvalue = json_object_array_get_idx(value, 0);
                            type = json_object_get_type(jvalue);
                            if (type == json_type_int) {
                                szMaxDelayQuant = (size_t)json_object_get_int(jvalue);
                            }
                            jvalue = json_object_array_get_idx(value, 1);
                            type = json_object_get_type(jvalue);
                            if (type == json_type_int) {
                                ulLockDelayUnit = (unsigned long)json_object_get_int(jvalue);
                            }
                        }
                    }
                } else {
                    printf("Undefined type: %s ", key);
                    print_json_value(value);
                }
            }
            json_object_put(jobjTmp);
            json_tokener_reset(tok);
            bPosRst = 1;
        }
    }

    // Compatible setup for previous setup json
    if (pstServIp == NULL) {
        printf("WARNING: No server info. Force to use default setup.\n");
        pstServIp = (pstAddrInfo) malloc(sizeof(stAddrInfo));
        if (pstServIp != NULL) {
            sprintf(pstServIp[0].ipAddr, "%s", "127.0.0.1");
            sprintf(pstServIp[0].ipPort, "%s", "8080");
        } else {
            perror("set_env: default malloc(pstServIp)");
            exit(EXIT_FAILURE);             
        }
    }

    // Print Summary
    printf("=== Configuration Summary ===\n");
    printf("%s-thread ", (nThreads == 1)? "Single" : "Multi");
    switch(kv511Type) {
        case KV511CLI:
            printf("client\n");
            if (nThreads > 1) {
                printf("# of Threads: %d\n", nThreads);
            }
            printf("ServerInfo: %s:%s\n", pstServIp[0].ipAddr, pstServIp[0].ipPort);
            break;
        case KV511SVR:
        case KV511SVR_STORE:
            printf("server\n");
            for (int i=0; i<nServResource; i++) {
                printf("ServerInfo[%d]: %s port\n", i, pstServIp[i].ipPort);
            }
            printf("LockDelayInfo: %zu quantum(max), unit delay %lu ms\n", szMaxDelayQuant, ulLockDelayUnit);
            break;
        default:
            printf("unknown\n");
            exit(0);
    }
    printf("# of Max sessions: %d\n", nMaxSessions);

    if (line != NULL) free(line);
    if (pStrBuf != NULL) free(pStrBuf);
    json_tokener_free(tok);
    fclose(fpConfig);
    return jobj;
}

void* process_session(void *recvfd) {
    session_t sockfd = (session_t)recvfd;

    /* check current time */
    struct timeval tv;

    /* Prepare string buffer for receiving packet bytes from socket */
    char dummy = 0;
    int nOffset, nPrevPos, nLastPos = -1;
    size_t szBuf = 0;
    char *pRecvBuf = get_strBuf(NULL, &szBuf, &nLastPos, &dummy, 1);
    ssize_t nBytes;

    /* JSON tokener */
    json_tokener *tok = NULL;
    json_object *jobj = NULL;
    int bPosRst = 0;

    /* Response Message */
    const char *pResult = NULL;

    if (pRecvBuf == NULL) {
        printf("[%s:%d] Error in allocating recvBuf (session: %d)\n", __func__, __LINE__, sockfd);
        close(sockfd);
        pthread_exit(NULL);
    }

    /* Create JSON tokener : parsing rule is based on strict mode */
    if ((tok = json_tokener_new()) == NULL) {
        printf("[%s:%d] Error in creating json tokener (session: %d)\n", __func__, __LINE__, sockfd);
        free(pRecvBuf);
        close(sockfd);
        pthread_exit(NULL);
    }
    json_tokener_set_flags(tok, JSON_TOKENER_STRICT);

    nPrevPos = 0;
    nLastPos = 0;
    bPosRst = 1;
    while((nBytes = recv(sockfd, pRecvBuf + nLastPos, szBuf - nLastPos, 0)) > 0) {
        json_object *jobjTmp = NULL;

        if (bPosRst) {
            nPrevPos = 0;
            bPosRst = 0;
        } else {
            nPrevPos = nLastPos;
        }
        nLastPos += nBytes;

        if(nLastPos == szBuf) {
            pRecvBuf = get_strBuf(pRecvBuf, &szBuf, &nLastPos, &dummy, 1);
            if (pRecvBuf == NULL) {
                perror("get_strBuf in recv");
                break;
            }
            nLastPos--;
        }

        jobjTmp = json_getJobj(tok, pRecvBuf + nPrevPos, nLastPos - nPrevPos, &nOffset, bDebug);
        if (jobjTmp != NULL) {
            enum json_type type;
            json_object *jDesc, *jRes = NULL;
            int nRemain = nLastPos - nPrevPos - nOffset;
            if (nRemain > 0 && pRecvBuf[nPrevPos + nOffset] == 0) {
                nOffset++;
                nRemain--;
            }
            memmove(pRecvBuf, pRecvBuf + nPrevPos + nOffset, nRemain);
            nLastPos = nRemain;

            json_object_object_foreach(jobjTmp, key, val) {
                jRes = json_object_new_object();
                type = json_object_get_type(val);
                if (strcmp(key, "get") == 0) {
                    if (type == json_type_string) {
                        // Process cache GET API request
                        lock(&kvCacheLock);
                        jDesc = get(-1, (_key_t*)json_object_get_string(val));                      
                        unlock(&kvCacheLock);

                        // Create Response
                        if (jDesc != NULL) {
                            json_object_object_add(jRes, "success", jDesc);
                        } else {
                            jDesc = json_object_new_string("there is no key.");
                            json_object_object_add(jRes, "fail", jDesc);
                        }
                    } else {
                        if (type == json_type_null) {
                            jDesc = json_object_new_string("empty parameter in get");
                        } else {
                            jDesc = json_object_new_string("invalid type");
                        }
                        json_object_object_add(jRes, "fail", jDesc);
                    }
                } else if (strcmp(key, "put") == 0) {
                    if (type == json_type_object) {
                        struct lh_entry *pPutObj = json_object_get_object(val)->head;
                        if (pPutObj) {
                            char *pPutReqKey = (char *)pPutObj->k;
                            struct json_object *pPutReqValue = (struct json_object*)pPutObj->v;
                            int res = 0;
                            size_t quantDelay = 0;

                            // Process cache PUT API request
                            lock(&kvCacheLock);
                            res = put(-1, pPutReqKey, pPutReqValue);
                            unlock(&kvCacheLock);

                            // Create Response
                            switch(res) {
                                case -1: /* invalid key */
                                    jDesc = json_object_new_string("invalid key");
                                    json_object_object_add(jRes, "fail", jDesc);
                                    break;
                                case -2: /* invalid value */
                                    jDesc = json_object_new_string("invalid value");
                                    json_object_object_add(jRes, "fail", jDesc);
                                    break;
                                case -3: /* cache full */
                                    jDesc = json_object_new_string("cache full");
                                    json_object_object_add(jRes, "fail", jDesc);
                                    break;
                                default:
                                    if (res == 0) {
                                        /* success : new */
                                        jDesc = json_object_new_string("new");
                                        json_object_object_add(jRes, "success", jDesc);
                                    } else if (res <= szKV511Cache) {
                                        /* success : update */
                                        jDesc = json_object_new_string("update");
                                        json_object_object_add(jRes, "success", jDesc);
                                    } else {
                                        /* unexpected error */
                                        jDesc = json_object_new_string("unexpected error");
                                        json_object_object_add(jRes, "fail", jDesc);
                                    }
                                    break;
                            }
                        }
                    } else {
                        jDesc = json_object_new_string("invalid format");
                        json_object_object_add(jRes, "fail", jDesc);
                    }
                } else {
                    printf("%s:%s\n", key, json_object_to_json_string(val));
                }

                pResult = json_object_to_json_string(jRes);
                if (send(sockfd, pResult, strlen(pResult), 0) == -1) {
                    perror("send");
                }
                json_object_put(jRes);
            }
            json_object_put(jobjTmp);
            json_tokener_reset(tok);
            bPosRst = 1;
        }
    }

    if(tok != NULL) json_tokener_free(tok);
    if(pRecvBuf != NULL) free(pRecvBuf);
    close(sockfd);
    pthread_exit(NULL);
}

// get sockaddr, IPv4 or IPv6:
void* get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void start_server(session_t servfd) {
    session_t new_fd;  // new connection on new_fd
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_storage their_addr; // connector's address information
    socklen_t sin_size;
    //struct sigaction sa;
    int rv, yes=1;
    char s[INET6_ADDRSTRLEN];

    while(1) {  // main accept() loop
        pthread_t tid = 0;
        sin_size = sizeof(their_addr);
        new_fd = accept(servfd, (struct sockaddr *)&their_addr, &sin_size);
        if (new_fd == -1) {
            perror("accept");
            continue;
        }

        inet_ntop(their_addr.ss_family,
            get_in_addr((struct sockaddr *)&their_addr),
            s, sizeof s);
        printf("server: got connection from %s\n", s);

        if (pthread_create(&tid, NULL, process_session, (void *)new_fd) != 0) {
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
        pthread_detach(tid);
    }
}

session_t init_server(void) {
    session_t sockfd;  // listen on sock_fd
    struct addrinfo hints, *servinfo, *p;
    int rv, yes = 1;

    if (kv511Type == KV511SVR) {
        kv511 = (pstkv511Cache)malloc(sizeof(stkv511Cache) * szKV511Cache);
        if (kv511 == NULL) {
            perror("server: malloc(kv511)");
            exit(EXIT_FAILURE);
        }
        // Cache Initialization
        for (int i=0; i < szKV511Cache; i++) {
            memset(kv511[i].key, 0, MAX_KEYLEN);
            kv511[i].pVal = NULL;
            kv511[i].szStoreLen = 0;
            kv511[i].nTimeStamp = 0ll;
        }
        // Prepare for cache lock
        lock_init(&kvCacheLock, ulLockDelayUnit, szMaxDelayQuant);

        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE; // use my IP

        if ((rv = getaddrinfo(NULL, pstServIp[0].ipPort, &hints, &servinfo)) != 0) {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            exit(EXIT_FAILURE);
        }

        // loop through all the results and bind to the first we can
        for(p = servinfo; p != NULL; p = p->ai_next) {
            if ((sockfd = socket(p->ai_family, p->ai_socktype,
                    p->ai_protocol)) == -1) {
                perror("server: socket");
                continue;
            }
            if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
                    sizeof(int)) == -1) {
                perror("setsockopt");
                exit(EXIT_FAILURE);
            }
            if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
                close(sockfd);
                perror("server: bind");
                continue;
            }
            break;
        }
        freeaddrinfo(servinfo); // all done with this structure

        if (p == NULL)  {
            fprintf(stderr, "server: failed to bind\n");
            exit(EXIT_FAILURE);
        }

        if (listen(sockfd, nMaxSessions) == -1) {
            perror("listen");
            exit(EXIT_FAILURE);
        }

        //struct sigaction sa;
        // sa.sa_handler = sigchld_handler; // reap all dead processes
        // sigemptyset(&sa.sa_mask);
        // sa.sa_flags = SA_RESTART;
        // if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        //     perror("sigaction");
        //     exit(1);
        // }

    } else if (kv511Type == KV511SVR_STORE) {

    } else {
        printf("server: unknown type %d", kv511Type);
        exit(1);
    }
    printf("server: waiting for connections... (session: %d)\n", sockfd);

    return sockfd;
}

void close_server(session_t servfd) {
    // Finalize operations here...
    close(servfd);
    lock_close(&kvCacheLock);
    if(kv511 != NULL) free(kv511);
    printf("server: closed\n");
}

/*returns the number of threads*/
int init_client()
{
    int client_len;
    char buf[MAXBUF];
    char rbuf[MAXBUF];
    int number_of_threads;
    
    
    pthread_t thread[MAX_TH];
    int input_fd[MAX_TH];
    
    
    printf("1\n");
    
    int i;
    char filename[80];
    
    printf("Number of threads?\n");
    scanf("%d",&number_of_threads);
    if(number_of_threads>MAX_TH)number_of_threads=MAX_TH;

    return number_of_threads;

}



void *read_and_request(int socket_num,struct request*  r)
{
    char buf[70];
    int len,client_len;
    int server_sockfd;
    static struct sockaddr_in serveraddr;
    clock_t begin, now;
    int index,i;
    index=0;
    
    while(1){
        int time;
        
        
        if ((server_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("error :");
            exit(0);
        }
        
        
        server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
        serveraddr.sin_family = AF_INET;
        serveraddr.sin_addr.s_addr = inet_addr(pstServIp[0].ipAddr);
        serveraddr.sin_port = htons(atoi(pstServIp[0].ipPort));
        
        client_len = sizeof(serveraddr);
        if (connect(server_sockfd, (struct sockaddr *)&serveraddr, client_len) < 0)
        {
            perror("connect error :");
            
            exit(0);
        }
        //making 10 requests per session

        
        for(i=1;i<=10;i++)
        {
            memset(buf,0x00,70);
            
            if(r[index].type==0)
            {
                get(server_sockfd,r[index].key);
                
            }
            if(r[index].type==1)
            {
                
                put(server_sockfd,r[index].key,json_object_new_string(r[index].value));
            }
            index++;
            
        }
        close(server_sockfd);
    }
}


void start_client(int i)
{
    char filename[20];
    int input_fd[MAX_TH];
    pthread_t thread[MAX_TH];
    
    sprintf(filename,"./input_file_for_thread_%d",i);
    input_fd[i]=open(filename,O_RDONLY);
    if(pthread_create(&thread[i],NULL,read_and_request,(void *)&input_fd[i])!=0)
    {
        
        perror("pthread create error");
        return;
    }
    (void) pthread_join(thread[i],NULL);

}

val_t* get(session_t sid, _key_t *k) {
    json_object *jobj = NULL;
    json_object *jkey = NULL; 
    json_object *jval = NULL;

    if (k == NULL) {
        return NULL;
    }

    if (kv511Type == KV511CLI) {
        if (sid < 0) {
            return NULL;
        }
        jobj = json_object_new_object();
        jkey = json_object_new_string((char *)k);
        json_object_object_add(jobj, "get", jkey);

        // Add snd/rcv here
        write(sid, json_object_to_json_string(jobj),strlen(json_object_to_json_string(jobj)));

        // Release object
        json_object_put(jobj);
    } else {
        // Server : Try to find key in cahce and return value to server
        if (bDebug) {
           printf("get %s\n", k);
        }

        // Simple Implementation : search in O(n)
        for(int i=0; i < szKV511Cache; i++) {
            if (strcmp(kv511[i].key, k) == 0) {
                jval = json_object_new_string(kv511[i].pVal);
                break;
            }
        }
        llLogicalTimeStamp++;
    }

    return (val_t*)jval;
}

int put(session_t sid, _key_t *k, val_t *v) {
    enum json_type type;
    json_object *jobj = NULL;
    json_object *jput = NULL;
    json_object *jval = (json_object *)v;
    int nRes = 0;
    if (k == NULL || v == NULL) {
        return 0;
    }

    if (kv511Type == KV511CLI) {
        if (sid < 0) {
            return 0;
        }
        jobj = json_object_new_object();
        jput = json_object_new_object();
        type = json_object_get_type(jval);
        switch(type) {
            case json_type_null:
            case json_type_boolean:
            case json_type_int:
            case json_type_double:
            case json_type_string:
                json_object_object_add(jput, (char *)k, jval);
                break;
            default:
                /* These are not value type */
                json_object_object_add(jput, (char *)k, NULL);
                break;
        }
        json_object_object_add(jobj, "put", jput);

        // Add snd/rcv here
        write(sid, json_object_to_json_string(jobj),strlen(json_object_to_json_string(jobj)));

        printf("%s\n", json_object_to_json_string(jobj));

        // Release object
        json_object_put(jobj);
    } else {
        enum json_type type;
        const char *pVal = NULL;
        size_t szLen = 0;
        int idx, nPos = 0;

        // Key format check
        szLen = strlen(k);
        if (szLen > MAX_KEYLEN) {
            return -1;
        }
        for (nPos=0; nPos<szLen && (isalpha(k[nPos]) || k[nPos]==' '); nPos++);
        if (nPos == szLen) {
            return -1;
        }
        for ( ; nPos<szLen && isdigit(k[nPos]); nPos++);
        if (nPos < szLen) {
            return -1;
        }
        // Value format check
        type = json_object_get_type(jval);
        if (type != json_type_string) {
            return -2;
        }
        // Server : Try to insert key/value in cahce and return status to server
        pVal = json_object_to_json_string(jval);
        szLen = strlen(pVal);

        if (bDebug) {
            printf("put %s:%s (sz: %zu)\n", k, pVal, szLen);
        }
        // Simple Implementation : search in O(2n)
        for(idx=0; idx < szKV511Cache; idx++) {
            if (strcmp(kv511[idx].key, k) == 0) {
                // Update! Find an existing entry
                if (kv511[idx].pVal != NULL) {
                    if (kv511[idx].szStoreLen <= szLen) {
                        free(kv511[idx].pVal);
                        kv511[idx].pVal = NULL;
                    }
                }
                break;
            }
        }
        if (idx == szKV511Cache) {
            // New! Find an empty entry
            for(idx=0; idx < szKV511Cache; idx++) {
                if (kv511[idx].nTimeStamp == 0) {
                    strncpy(kv511[idx].key, k, MAX_KEYLEN);                    
                    break;
                }
            }
            nRes = 0;
        } else {
            // Return the descriptor of updated entry (idx+1)
            nRes = idx+1;
        }
        if (idx == szKV511Cache) {
            // Cache is full.
            return -3;
        } else {
            struct timeval tv;
            if (kv511[idx].pVal == NULL) {
                kv511[idx].pVal = (char *)malloc(sizeof(char) * (szLen+1));
                kv511[idx].szStoreLen = szLen+1;
            }
            strncpy(kv511[idx].pVal, pVal, szLen+1);
            kv511[idx].nTimeStamp = ++llLogicalTimeStamp;
        }
    }
    return nRes;
}
