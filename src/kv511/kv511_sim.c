
/* Headers for Memory & String */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* Headers for networking */
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>

/* Headers for multi-thread */
#include <pthread.h>

#include "kv511.h"



/* buffer size and the max number of threads in client*/
#define MAXBUF 500
#define MAX_TH 100

/* Key-Value Store Cache structure */
#define MAX_KEYLEN 10
typedef struct {
    char key[MAX_KEYLEN];
    char *pDesc;
    long nTimeStamp;
} stkv511Cache, *pstkv511Cache;
static pstkv511Cache kv511 = NULL;

/* default configurations */
static int bClient = 0;
static int nThreads = 1;
static size_t szKV511Cache = 10;

/* In server, the number of threads depends on nMaxSessions */
static int nMaxSessions = 10;

/* IPv4-mapped IPv6 has 45 characters */
static char servIpAddr[INET6_ADDRSTRLEN];
#define IPPORT_MAXLEN 6
static char servIpPort[IPPORT_MAXLEN];

struct request{
    int type; //put:0, get:1
    int key; //integer between 0 and 1000
    int val; //integer between 0 and 2000
//    int time;  //closed-loop ver
};




int get_numThreads(void) {
    return nThreads;
}

size_t trim_trailingWhiteSpace(char *pStr, size_t szStrLen) {
    int nPos = 0;
    if (pStr == NULL || szStrLen == 0){
        return 0;
    }
    nPos = szStrLen - 1;
    while(nPos > 0 && isspace(pStr[nPos])) nPos--;

    if (nPos < 0) {
        nPos = -1;
    }
    return nPos + 1;
}

char* get_strBuf(char *restrict pBuf, size_t *restrict pszBuf, int *restrict pnLastPos, char *restrict pNewline, size_t szNewline) {
    const size_t szIncBuf = BUFLEN;
    size_t szBufRemain = 0;
    char *pNew = NULL;
    int nLastPos = 0;

    if (pszBuf == NULL || pnLastPos == NULL || pNewline == NULL || szNewline == 0) {
        printf("[%s:%d] param error (pszBuf: %x, pnLastPos: %x, pNewline: %x, szNewline: %x)", \
            __func__, __LINE__, (unsigned int)pszBuf, (unsigned int)pnLastPos, (unsigned int)pNewline, (unsigned int)szNewline);
        return NULL;
    }

    if (pBuf == NULL) {
        szBufRemain = 0;
        nLastPos = 0;
    } else {
        nLastPos = *pnLastPos;
        szBufRemain = *pszBuf - nLastPos;
    }
    
    if (szBufRemain < szNewline) {
        size_t szNewBuf = nLastPos + szIncBuf;

        if (szNewline > szNewBuf) {
            szNewBuf = szNewline;
        }
        pNew = (char *)realloc(pBuf, sizeof(char) * szNewBuf);
        if (pNew != NULL) {
            *pszBuf = szNewBuf;
        } else {
            printf("[%s:%d] pNew realloc error", __func__, __LINE__);
        }
    } else {
        pNew = pBuf;
    }

    memcpy(pNew + nLastPos, pNewline, sizeof(char) * szNewline);
    *pnLastPos = nLastPos + szNewline;

    return pNew;
}

json_object* set_env(const char *pJSONEnvFileName) {
    FILE *fpConfig = NULL;
    json_tokener *tok = NULL;
    json_object *jobj = NULL;

    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    size_t szBuf = 0;
    char *pStrBuf = NULL;

    int bPosRst = 0;
    int nPrevPos = 0;
    int nLastPos = 0;
    int nOffset = -1;

    /* Open JSON configuration file */
    if ((fpConfig = fopen(pJSONEnvFileName, "r")) == NULL) {
        printf("[%s:%d] Error in opening %s\n", __func__, __LINE__, pJSONEnvFileName);
        return NULL;
    }
    
    /* Create JSON tokener : parsing rule is based on strict mode */
    if ((tok = json_tokener_new()) == NULL) {
        printf("[%s:%d] Error in create json tokener\n", __func__, __LINE__);
        return NULL;
    }
    json_tokener_set_flags(tok, JSON_TOKENER_STRICT);

    /* read each line from JSON configuration file */
    bPosRst = 1;
    while((read = getline(&line, &len, fpConfig)) != -1) {
        json_object *jobjTmp = NULL;
        /* 
         * At first, trim trailing whitespaces from string 
         * If there is no line, skip to the next line
         */
        size_t szStrTrim = trim_trailingWhiteSpace(line, strlen(line));
        if (szStrTrim == 0) continue;

        /*
         * JSON tokener does not take care of string buffer.
         * Therefore, we need to keep the entire string until JSON tokener returns JSON object.
         * CAVEAT: For each parsing step, we are going to feed each line instead of the entire string.
         */
        if (bPosRst) {
            nPrevPos = 0;
            bPosRst = 0;
        } else {
            nPrevPos = nLastPos;
        }
        pStrBuf = get_strBuf(pStrBuf, &szBuf, &nLastPos, line, szStrTrim);
        jobjTmp = json_getJobj(tok, pStrBuf + nPrevPos, nLastPos - nPrevPos, &nOffset);
        if (jobjTmp != NULL) {
            /*
             * If JSON object is created by JSON tokener,
             * we do not need to keep the original string.
             * Restore buffer space for the following strings.
             */
            enum json_type type;
            int nRemain = nLastPos - nPrevPos - nOffset;
            memmove(pStrBuf, pStrBuf + nPrevPos + nOffset, nRemain);
            nLastPos = nRemain;

            json_object_object_foreach(jobjTmp, key, value) {
                type = json_object_get_type(value);
                if (strcmp(key, "type") == 0) {
                    if (type == json_type_string) {
                        bClient = (strcmp(json_object_get_string(value), "client") == 0);
                    }
                } else if (strcmp(key, "threads") == 0) {
                    if (type == json_type_int) {
                        nThreads = json_object_get_int(value);
                    }
                }  else if (strcmp(key, "servip") == 0) {
                    // If this is client, ipaddr indicates server's ip address
                    // Otherwise, ignore.
                    if (type == json_type_string && json_object_get_string_len(value) <= INET6_ADDRSTRLEN) {
                        sprintf(servIpAddr, "%s", json_object_get_string(value));                    
                    }
                } else if (strcmp(key, "port") == 0) {
                    // Port indicates server's listening port
                    if (type == json_type_int) {
                        sprintf(servIpPort, "%d", json_object_get_int(value));
                    }
                } else if (strcmp(key, "max_session") == 0) {
                    // How many session(socket) can we use at the same time?
                    if (type == json_type_int) {
                        nMaxSessions = json_object_get_int(value);
                    }
                } else if (strcmp(key, "cache_size") == 0) {
                    // How many session(socket) can we use at the same time?
                    if (type == json_type_int) {
                        szKV511Cache = json_object_get_int(value);
                    }
                } else if (strcmp(key, "request") == 0) {
                    // Extract request JSON value and deliver it to callee
                    jobj = json_object_get(value);
                } else {
                    printf("type: %s ", key);
                    print_json_value(value);
                }
            }
            json_object_put(jobjTmp);
            json_tokener_reset(tok);
            bPosRst = 1;
        }
    }

    // Print Summary
    printf("=== Configuration Summary ===\n");
    printf("%s-thread %s\n", (nThreads == 1)? "Single" : "Multi", (bClient) ? "client": "server");
    printf("ServerInfo: ");
    if (bClient) {
        printf("%s:%s\n", servIpAddr, servIpPort);
    } else {
        printf("%s port\n", servIpPort);
    }
    printf("# of Max sessions: %d\n", nMaxSessions);
    if (nThreads > 1 && bClient) {
        printf("# of Threads: %d\n", nThreads);
    }

    if (line != NULL) free(line);
    if (pStrBuf != NULL) free(pStrBuf);
    json_tokener_free(tok);
    fclose(fpConfig);
    return jobj;
}

/* parses a string to request*/
void parse_request(char* str,struct request *req)
{
    int i;
    if(str[0]=='p'){
        req->type=0;
        req->key=atoi(&str[2]);
    }
    else if(str[0]=='g'){
        req->type=1;
        for(i=2;i<=10;i++)
        {
            if(str[i]=='_'){
                str[i]=0;
                req->key=atoi(&str[2]);
                req->val=atoi(&str[i+1]);
                
        }
        
    }
    else{
        req->type=str[0];
    }    

        
}

/*processes a request*/
void process_reqest(struct request* req) {
    
    
        
        
        
}
    
    
/* processes a session(10 requestsi)*/
void* process_session(void *recvfd) {
    session_t sockfd = (session_t)recvfd;
    
    int i,j;
    char *buf[11];
    struct request req;
    for(i=1;i<=10;i++)
    {
        read(recvfd,buf,11);
        parse_request(buf,&req);
        process_reqest(&req);
    
    }
    
    
    
}

// get sockaddr, IPv4 or IPv6:
void* get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void start_server(session_t servfd) {
    session_t new_fd;  // new connection on new_fd
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_storage their_addr; // connector's address information
    socklen_t sin_size;
    //struct sigaction sa;
    int rv, yes=1;
    char s[INET6_ADDRSTRLEN];

    while(1) {  // main accept() loop
        pthread_t tid = 0;
        sin_size = sizeof(their_addr);
        new_fd = accept(servfd, (struct sockaddr *)&their_addr, &sin_size);
        if (new_fd == -1) {
            perror("accept");
            continue;
        }

        inet_ntop(their_addr.ss_family,
            get_in_addr((struct sockaddr *)&their_addr),
            s, sizeof s);
        printf("server: got connection from %s\n", s);

        if (pthread_create(&tid, NULL, process_session, (void *)new_fd) != 0) {
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
        pthread_detach(tid);
    }
}

session_t init_server(void) {
    session_t sockfd;  // listen on sock_fd
    struct addrinfo hints, *servinfo, *p;
    int rv, yes = 1;

    kv511 = (pstkv511Cache)malloc(sizeof(stkv511Cache) * szKV511Cache);
    if (kv511 == NULL) {
        perror("server: malloc(kv511)");
        exit(EXIT_FAILURE);
    }

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; // use my IP

    if ((rv = getaddrinfo(NULL, servIpPort, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        exit(EXIT_FAILURE);
    }

    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) == -1) {
            perror("server: socket");
            continue;
        }
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
                sizeof(int)) == -1) {
            perror("setsockopt");
            exit(EXIT_FAILURE);
        }
        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("server: bind");
            continue;
        }
        break;
    }
    freeaddrinfo(servinfo); // all done with this structure

    if (p == NULL)  {
        fprintf(stderr, "server: failed to bind\n");
        exit(EXIT_FAILURE);
    }

    if (listen(sockfd, nMaxSessions) == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //struct sigaction sa;
    // sa.sa_handler = sigchld_handler; // reap all dead processes
    // sigemptyset(&sa.sa_mask);
    // sa.sa_flags = SA_RESTART;
    // if (sigaction(SIGCHLD, &sa, NULL) == -1) {
    //     perror("sigaction");
    //     exit(1);
    // }

    printf("server: waiting for connections... (session: %d)\n", sockfd);

    return sockfd;
}

void close_server(session_t servfd) {
    // Finalize operations here...
    close(servfd);
    if(kv511 != NULL) free(kv511);
    printf("server: closed\n");
}

/*returns the number of threads*/
int init_client()
{
    int client_len;
    char buf[MAXBUF];
    char rbuf[MAXBUF];
    int number_of_threads;
    
    
    pthread_t thread[MAX_TH];
    int input_fd[MAX_TH];
    
    
    printf("1\n");
    
    int i;
    char filename[80];
    
    printf("Number of threads?\n");
    scanf("%d",&number_of_threads);
    if(number_of_threads>MAX_TH)number_of_threads=MAX_TH;

    return number_of_threads;

}



void *read_and_request(int socket_num,struct request*  r)
{
    char buf[70];
    int len,client_len;
    int server_sockfd;
    static struct sockaddr_in serveraddr;
    clock_t begin, now;
    int index,i;
    index=0;
    
    while(1){
        int time;
        
        
        if ((server_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("error :");
            exit(0);
        }
        
        
        server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
        serveraddr.sin_family = AF_INET;
        serveraddr.sin_addr.s_addr = inet_addr(servIpAddr);
        serveraddr.sin_port = htons(atoi(servIpPort));
        
        client_len = sizeof(serveraddr);
        if (connect(server_sockfd, (struct sockaddr *)&serveraddr, client_len) < 0)
        {
            perror("connect error :");
            
            exit(0);
        }
        
        
        for(i=1;i<=10;i++)
        {
            memset(buf,0x00,70);
            
            if(r[index].type==0)
            {
                get(server_sockfd,r[index].key);
                
            }
            if(r[index].type==1)
            {
                
                put(server_sockfd,r[index].key,json_object_new_string(r[index].value));
            }
            index++;
            
        }
        close(server_sockfd);
    }
}


void start_client(int i)
{
    char filename[20];
    int input_fd[MAX_TH];
    pthread_t thread[MAX_TH];
    
    sprintf(filename,"./input_file_for_thread_%d",i);
    input_fd[i]=open(filename,O_RDONLY);
    if(pthread_create(&thread[i],NULL,read_and_request,(void *)&input_fd[i])!=0)
    {
        
        perror("pthread create error");
        return;
    }
    (void) pthread_join(thread[i],NULL);

}

val_t* get(session_t sid, _key_t *t) {
    json_object *jobj = NULL;
    json_object *jkey = NULL; 

    if ((bClient && sid < 0) || t == NULL) {
        return NULL;
    }

    if (bClient) {
        jobj = json_object_new_object();
        jkey = json_object_new_string((char *)t);
        json_object_object_add(jobj, "get", jkey);

        // Add snd/rcv here
        write(sid, json_object_to_json_string(jobj),strlen(json_object_to_json_string(jobj)));

        // Release object
        json_object_put(jobj);
    } else {
        // Server : Try to find key in cahce and return value to server
        printf("get %s\n", t);
    }

    return NULL;
}

int put(session_t sid, _key_t *k, val_t *v) {
    enum json_type type;
    json_object *jobj = NULL;
    json_object *jput = NULL;
    json_object *jval = (json_object *)v;
    if ((bClient && sid < 0) || k == NULL || v == NULL) {
        return 0;
    }

    if (bClient) {
        jobj = json_object_new_object();
        jput = json_object_new_object();
        type = json_object_get_type(jval);
        switch(type) {
            case json_type_null:
            case json_type_boolean:
            case json_type_int:
            case json_type_double:
            case json_type_string:
                json_object_object_add(jput, (char *)k, jval);
                break;
            default:
                /* These are not value type */
                json_object_object_add(jput, (char *)k, NULL);
                break;
        }
        json_object_object_add(jobj, "put", jput);

        // Add snd/rcv here
        write(sid, json_object_to_json_string(jobj),strlen(json_object_to_json_string(jobj)));

        printf("%s\n", json_object_to_json_string(jobj));

        // Release object
        json_object_put(jobj);
    } else {
        // Server : Try to insert key/value in cahce and return status to server
        printf("put %s:%s\n", k, json_object_to_json_string(jval));   
    }

    return 0;
}
