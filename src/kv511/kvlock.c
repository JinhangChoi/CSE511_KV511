// #define LOCK_DBG

#ifdef LOCK_DBG
#include <stdio.h>
#endif
/* memory management */
#include <stdlib.h>
#include <string.h>

/* signal handling */
#include <signal.h>

#include "kvlock.h"

// Based on T. Anderson's TPDS'90 paper,
// I decide to use spin on read with delay after lock release.
// But, how long should I give delay? Test monotonous backoff
#define spin_on_read_with_delay(lock, delay, start, end, unit) \
    while (lock == 1 || test_and_set(&lock, 1) == 1) { \
        while (lock == 1);  \
        start = (start % end) + 1; \
        delay = start * unit; \
        usleep(delay);   \
    }

INLINE int test_and_set(int *old, int new) {
    int tmp = *old;
    *old = new;
    return tmp;
}

void lock_init(lock_t *m, unsigned long ulDelayUnit, size_t szMaxQuant) {
    lock_t tmp = {0, NULL, PTHREAD_MUTEX_INITIALIZER, 0, 0, 0, STAILQ_HEAD_INITIALIZER(m->q)};
    // initialize lock structure
    memcpy(m, &tmp, sizeof(lock_t));
    // initialize mutex
    pthread_mutex_init(&m->mutex, NULL);
    // initialize queue in lock
    STAILQ_INIT(&m->q);
    // initialize the unit time of delay
    m->unitDelay = ulDelayUnit;
    m->szMaxQuant = szMaxQuant;
}

void lock_close(lock_t *m) {
    struct queue_t *pPrev, *pNext;
    pthread_mutex_destroy(&m->mutex);
    pPrev = STAILQ_FIRST(&m->q);
    while (pPrev != NULL) {
            pNext = STAILQ_NEXT(pPrev, entries);
            free(pPrev);
            pPrev = pNext;
    }
}

void lock(lock_t *m) {
    useconds_t delay = 0;
    useconds_t unitDelay = m->unitDelay;
    size_t szLast = m->szMaxQuant;
    int bCheckQ = 0;

    spin_on_read_with_delay(m->guard, delay, m->szLastQuant, szLast, unitDelay);

    // It is not easy to keep flags from race condition in user space.
    // Enforce lock to enqueue tasks without exception
    {
        int sig, i=0;
        sigset_t sigmask;
        struct queue_t *tmp, *p = NULL;
        pthread_t tid = pthread_self();

        // To realize setpark, the order of enqueue need to be kept
        // and setpark() should not suffer from wakeup/waiting race condition.
        // As a result, there is no way but using mutex at least this location
        // in user space
        pthread_mutex_lock(&m->mutex);

        // check whether or not our queue is empty.
        bCheckQ = STAILQ_EMPTY(&m->q);

        // enqueue thread ID
        p = (struct queue_t *)malloc(sizeof(struct queue_t));
        p->tid = tid;
        STAILQ_INSERT_TAIL(&m->q, p, entries);

        if (!bCheckQ && m->tParkId == NULL) {
            m->tParkId = tid;           // setpark: this thread is going to park.
        }
        pthread_mutex_unlock(&m->mutex);        

        if (bCheckQ) {                  // alone: do not want to wait for others.
            m->guard = 0;
        } else {                        // contention: need to wait for others.
            // mask signal
            sigemptyset(&sigmask);
            sigaddset(&sigmask, SIGCONT);
            pthread_sigmask(SIG_BLOCK, &sigmask, (sigset_t *)NULL);
            
            if (m->tParkId != tid) {
                m->tParkId = NULL;      // clear setpark
#ifdef LOCK_DBG
                printf("I'm here!!!! (tid: %x, delay: %d)\n", tid, delay);
#endif
                raise(SIGCONT);
            }
#ifdef LOCK_DBG
            else {
                printf("setpark (tid: %x, delay: %d)\n", tid, delay);
            }
#endif                        
            m->guard = 0;
            sigwait(&sigmask, &sig);    // park: in other words, wait here!
            pthread_sigmask(SIG_UNBLOCK, &sigmask, (sigset_t *)NULL);
            if (m->tParkId == tid) {
                m->tParkId = NULL;
            }
#ifdef LOCK_DBG
            STAILQ_FOREACH(tmp, &m->q, entries) i++;
            printf("hi! (qcnt: %d, sig: %d, tid: %x)\n", i, sig, tid);
#endif            
        }
    }
}

void unlock(lock_t *m) {
    useconds_t delay = 0;
    useconds_t unitDelay = m->unitDelay;
    size_t szLast = m->szMaxQuant;
    pthread_t mytid = pthread_self();
    int bCheckQ = 0;
    
    spin_on_read_with_delay(m->guard, delay, m->szLastQuant, szLast, unitDelay);

    // It is not easy to keep flags from race condition in user space.
    // Enforce unlock to check if queue is empty without exception
    pthread_mutex_lock(&m->mutex);
    bCheckQ = STAILQ_EMPTY(&m->q);
    pthread_mutex_unlock(&m->mutex);

    if (bCheckQ) {
        if(m->tParkId == mytid) {
            m->tParkId = NULL;      // clear setpark
        }
#ifdef LOCK_DBG
        printf("unlock: release (delay: %d, tid: %d)\n", delay, mytid);
#endif
        // let go of lock; no one wants it
        m->guard = 0;
    } else {
        struct queue_t *p = NULL;

        pthread_mutex_lock(&m->mutex);
        if ((p = STAILQ_FIRST(&m->q))!= NULL) {
            pthread_t tid = p->tid;
            STAILQ_REMOVE_HEAD(&m->q, entries);
            pthread_mutex_unlock(&m->mutex);
            free(p);
            if (mytid == tid) {
                p = STAILQ_FIRST(&m->q);            // peek queue to wake up the next candidate
                if (p != NULL) {
                    tid = p->tid;                   
                    if(m->tParkId == tid) {
                        m->tParkId = NULL;          // clear setpark
                    }
                }
            } else if(m->tParkId == mytid) {
                m->tParkId = NULL;                  // clear setpark
            }
#ifdef LOCK_DBG
            printf("unlock: wake %x up at %x (delay: %d)\n", tid, mytid, delay);
            if(pthread_kill(tid, SIGCONT) != 0) {   // unpark: in other words, wake it up!
                perror("pthread_kill");
            }
#else
            pthread_kill(tid, SIGCONT);             // unpark: in other words, wake it up!
#endif
        } else {
            if(m->tParkId == mytid) {
                m->tParkId = NULL;                  // clear setpark
            }            
            pthread_mutex_unlock(&m->mutex);
        }
        m->guard = 0;
    }
}