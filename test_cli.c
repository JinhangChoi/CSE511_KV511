/*
** client.c -- a stream socket client demo
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#define PORT "8080" // the port client will be connecting to 

#define MAXDATASIZE 100 // max number of bytes we can get at once 

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(int argc, char *argv[])
{
    FILE *fpRequest = NULL;
    char *pReqFileName = "json/testRequests.json";
    int sockfd, numbytes;  
    char buf[MAXDATASIZE];
    struct addrinfo hints, *servinfo, *p;
    int rv;
    char s[INET6_ADDRSTRLEN];

    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    if (argc != 2) {
        fprintf(stderr,"usage: client hostname\n");
        exit(1);
    }

    if ((fpRequest = fopen(pReqFileName, "r")) == NULL) {
        fprintf(stderr,"fopen: %s\n", pReqFileName);
        exit(1);
    }

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if ((rv = getaddrinfo(argv[1], PORT, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }

        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("client: connect");
            continue;
        }

        break;
    }

    if (p == NULL) {
        fprintf(stderr, "client: failed to connect\n");
        return 2;
    }

    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
            s, sizeof s);
    printf("client: connecting to %s\n", s);

    freeaddrinfo(servinfo); // all done with this structure

    // Try to send test requests
    while((read = getline(&line, &len, fpRequest)) != -1) {

        // Remove trailing whitespaces
        for(int i = read-1; i>=0 && isspace(line[i]); i--) {
            line[i] = 0;
        }
        read = strlen(line);

        // Send request
        if ((numbytes = send(sockfd, line, read, 0)) == -1) {
            perror("send");
            exit(1);
        }
        printf("send %d bytes\n", numbytes);

        // Recv response
        if((numbytes = recv(sockfd, buf, MAXDATASIZE-1, 0)) > 0) {
            buf[numbytes] = '\0';
            printf("client: received '%s'\n",buf);
        }
        if (numbytes == -1) {
            perror("recv");
            exit(1);
        }
    }

    close(sockfd);

    return 0;
}
